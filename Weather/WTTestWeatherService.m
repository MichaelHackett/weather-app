//
//  WTTestWeatherService.m
//  Weather
//

#import "WTTestWeatherService.h"

#import "WTWeather.h"
#import "NSOperation+WTDisplayThreadOperations.h"
#import <Foundation/Foundation.h>



static NSString* const ServiceURLString =
    @"http://www.raywenderlich.com/demos/weather_sample/weather.php?format=json";

NSString* const WTHTTPErrorDomain = @"HTTP";

static NSUInteger const HTTPStatus_OK = 200;



static void showNetworkActivityIndicator(BOOL showIndicator)
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = showIndicator;
}

static NSError* errorFromHttpResponse(NSHTTPURLResponse* httpResponse)
{
    NSURL* responseUrl = [httpResponse URL];
    NSDictionary* extraInfo = @{
        NSURLErrorFailingURLErrorKey: responseUrl,
        NSURLErrorFailingURLStringErrorKey: [responseUrl absoluteString],
    };
    return [NSError errorWithDomain:WTHTTPErrorDomain
                               code:httpResponse.statusCode
                           userInfo:extraInfo];
}




//
// Helpers for decoding JSON data returned from service.
//

@interface WTTestWeatherServiceConditions : NSObject
@end
@implementation WTTestWeatherServiceConditions
{
    NSDictionary* _conditionsData;
    NSDateFormatter* _dateParser;
}

- (instancetype)initWithConditionsData:(NSDictionary*)conditionsData
{
    self = [super init];
    if (self) {
        _conditionsData = [conditionsData copy];
        _dateParser = [[NSDateFormatter alloc] init];
        // TODO configure parser for data format
    }
    return self;
}

- (NSString*)stringValueForKey:(NSString*)key
{
    id obj = _conditionsData[key];
    return obj && [obj isKindOfClass:[NSString class]]
           ? (NSString*)obj
           : nil;
}

- (NSNumber*)integerValueForKey:(NSString*)key
{
    NSString* stringValue = [self stringValueForKey:key];
    return stringValue ? @([stringValue integerValue]) : nil;
}

- (NSArray*)arrayForKey:(NSString*)key
{
    id obj = _conditionsData[key];
    return obj && [obj isKindOfClass:[NSArray class]]
           ? (NSArray*)obj
           : nil;
}

- (id)firstValueOfArrayForKey:(NSString*)key
{
    id elem = [self arrayForKey:key][0];
    if ([elem isKindOfClass:[NSDictionary class]]) {
        return ((NSDictionary*)elem)[@"value"];
    }
    return nil;
}

- (NSNumber*)cloudCover { return [self integerValueForKey:@"cloudcover"]; }
- (NSNumber*)humidity { return [self integerValueForKey:@"humidity"]; }
- (NSNumber*)precipMM { return [self integerValueForKey:@"precipMM"]; }
- (NSNumber*)pressure { return [self integerValueForKey:@"pressure"]; }
- (NSNumber*)tempC { return [self integerValueForKey:@"temp_C"]; }
- (NSNumber*)tempF { return [self integerValueForKey:@"temp_F"]; }
- (NSNumber*)tempMaxC { return [self integerValueForKey:@"tempMaxC"]; }
- (NSNumber*)tempMaxF { return [self integerValueForKey:@"tempMaxF"]; }
- (NSNumber*)tempMinC { return [self integerValueForKey:@"tempMinC"]; }
- (NSNumber*)tempMinF { return [self integerValueForKey:@"tempMinF"]; }
- (NSNumber*)visibility { return [self integerValueForKey:@"visibility"]; }
- (NSNumber*)weatherCode { return [self integerValueForKey:@"weatherCode"]; }
- (NSString*)windDir16Point { return _conditionsData[@"winddir16Point"]; };
- (NSNumber*)windDirDegree { return [self integerValueForKey:@"winddirDegree"]; }
- (NSNumber*)windSpeedKmph { return [self integerValueForKey:@"windSpeedKmph"]; }
- (NSNumber*)windSpeedMiles { return [self integerValueForKey:@"windSpeedMiles"]; }

- (NSString*)observationTime { return [self stringValueForKey:@"observation_time"]; }

- (NSString*)weatherDescription { return [self firstValueOfArrayForKey:@"weatherDesc"]; }
- (NSString*)weatherIconURL { return [self firstValueOfArrayForKey:@"weatherIconUrl"]; }

- (NSDate*)date
{
    NSString* dateString = [self stringValueForKey:@"date"];
    return dateString ? [_dateParser dateFromString:dateString] : nil;
}

@end



@interface WTTestWeatherServiceResponse : NSObject
@end
@implementation WTTestWeatherServiceResponse
{
    NSDictionary* _response;
}

- (instancetype)initWithJsonObj:(id)jsonObj
{
    self = [super init];
    if (self) {
        if ([jsonObj isKindOfClass:[NSDictionary class]]) {
            _response = [((NSDictionary*)jsonObj)[@"data"] copy];
        } else {
            self = nil;
        }
    }
    return self;
}

- (WTTestWeatherServiceConditions*)currentConditions
{
    id conditions = _response[@"current_condition"];
    if ([conditions isKindOfClass:[NSArray class]]) {
        id currentConditions = ((NSArray*)conditions)[0];
        if ([currentConditions isKindOfClass:[NSDictionary class]]) {
            return [[WTTestWeatherServiceConditions alloc]
                    initWithConditionsData:currentConditions];
        }
    }
    return nil;
}

- (NSUInteger)countOfForecasts
{
    id forecastsData = _response[@"weather"];
    if ([forecastsData isKindOfClass:[NSArray class]]) {
        return [(NSArray*)forecastsData count];
    }
    return 0;
}

- (WTTestWeatherServiceConditions*)forecastAtIndex:(NSUInteger)index
{
    id forecastsData = _response[@"weather"];
    if ([forecastsData isKindOfClass:[NSArray class]]) {
        id forecastData = ((NSArray*)forecastsData)[index];
        if ([forecastData isKindOfClass:[NSDictionary class]]) {
            return [[WTTestWeatherServiceConditions alloc]
                    initWithConditionsData:forecastData];
        }
    }
    return nil;
}

@end



//
// Conversion from service response to application Weather model.
//

static WTWeatherReport* weatherReportFromTestWeatherServiceResponse(
        id jsonObj,
        NSError* __autoreleasing * error)
{
    WTTestWeatherServiceResponse* response =
        [[WTTestWeatherServiceResponse alloc] initWithJsonObj:jsonObj];
    if (response) {
        WTTestWeatherServiceConditions* conditionsData = [response currentConditions];
        if (conditionsData) {
            WTWeatherConditions* current = [[WTWeatherConditions alloc] init];
            current.weatherDescription = [conditionsData weatherDescription];
            current.tempC = [conditionsData tempC];

            NSMutableArray* forecasts = [[NSMutableArray alloc] init];
            for (NSUInteger i = 0; i < [response countOfForecasts]; i++) {
                WTTestWeatherServiceConditions* forecastData = [response forecastAtIndex:i];
                WTWeatherConditions* forecast = [[WTWeatherConditions alloc] init];
                forecast.weatherDescription = [forecastData weatherDescription];
                forecast.tempMinC = [forecastData tempMinC];
                forecast.tempMaxC = [forecastData tempMaxC];
                [forecasts addObject:forecast];
            }

            WTWeatherReport* weather = [[WTWeatherReport alloc] init];
            weather.currentConditions = current;
            weather.forecasts = forecasts;

            return weather;
        }
    }

    // Parsing failed --- return error.
    if (error) {
        *error = [NSError errorWithDomain:NSURLErrorDomain
                                     code:NSURLErrorCannotParseResponse
                                 userInfo:nil];
    }
    return nil;
}




//
// Web service class
//

@implementation WTTestWeatherService
{
    NSURLSession* _networkSession;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSURLSessionConfiguration* config =
            [NSURLSessionConfiguration ephemeralSessionConfiguration];
        config.allowsCellularAccess = YES;

        _networkSession = [NSURLSession sessionWithConfiguration:config];
    }
    return self;
}

- (void)retrieveWeatherDataAndOnCompletion:(void (^)(WTWeatherReport*))completionHandler
                                 onFailure:(void (^)(NSError*))failureHandler
{
    NSURL* serviceUrl = [NSURL URLWithString:ServiceURLString];
    NSURLSessionTask* task =
        [_networkSession dataTaskWithURL:serviceUrl
                       completionHandler:^(NSData* data,
                                           NSURLResponse* response,
                                           NSError* error)
    {
        [NSOperation onDisplayThreadPerformBlock:^{
            showNetworkActivityIndicator(NO);
        }];
        WTWeatherReport* weather = nil;
        if (!error) {
            NSInteger status = [(NSHTTPURLResponse*)response statusCode];
            if (status != HTTPStatus_OK) {
                error = errorFromHttpResponse((NSHTTPURLResponse*)response);
            } else {
                id jsonObj = [NSJSONSerialization JSONObjectWithData:data
                                                             options:0
                                                               error:&error];
                if (!error) {
                    weather = weatherReportFromTestWeatherServiceResponse(jsonObj, &error);
                }
            }
        }
        if (error) {
            failureHandler(error);
        } else {
            completionHandler(weather);
        }
    }];

    [NSOperation onDisplayThreadPerformBlock:^{
        showNetworkActivityIndicator(YES);
    }];
    [task resume];
}

@end
