//
//  WTFakeWeatherService.h
//  Weather
//

#import "WTWeatherService.h"
#import <Foundation/NSObject.h>

@interface WTFakeWeatherService : NSObject <WTWeatherService>

@end
