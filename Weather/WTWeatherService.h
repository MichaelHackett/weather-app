//
//  WTWeatherService.h
//  Weather
//

#import <Foundation/NSObject.h>

@class WTWeatherReport;
@class NSError, NSString;


// NSError domain for HTTP request errors; userInfo uses same NSURLError...Key
// dictionary entries as found in <Foundation/NSURLError.h> (for now).
extern NSString* const WTHTTPErrorDomain;


@protocol WTWeatherService <NSObject>

- (void)retrieveWeatherDataAndOnCompletion:(void(^)(WTWeatherReport* weather))completionHandler
                                 onFailure:(void(^)(NSError* error))failureHandler;

@end
