//
//  WTTableViewController.m
//  Weather
//
//  Created by Scott on 26/01/2013.
//  Updated by Joshua Greene 16/12/2013.
//
//  Copyright (c) 2013 Scott Sherwood. All rights reserved.
//

#import "WTWeatherConditionsListViewController.h"

#import "WTWeather.h"
#import "WTWeatherAnimationViewController.h"
#import "WTWeatherService.h"
#import "NSOperation+WTDisplayThreadOperations.h"


@interface WTWeatherConditionsListViewController ()
@property (strong, nonatomic) WTWeatherReport *weather;
@end

@implementation WTWeatherConditionsListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.toolbarHidden = NO;
    [self refreshList];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"WeatherDetailSegue"]){
        UITableViewCell *cell = (UITableViewCell *)sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        
        WTWeatherConditions* conditionsToDisplay;
        switch (indexPath.section) {
            case 0: {
                conditionsToDisplay = self.weather.currentConditions;
                break;
            }
            case 1: {
                conditionsToDisplay = self.weather.forecasts[(NSUInteger)indexPath.row];
                break;
            }
            default: {
                break;
            }
        }
        WTWeatherAnimationViewController *wac =
            (WTWeatherAnimationViewController *)segue.destinationViewController;
        wac.weatherConditions = conditionsToDisplay;
    }
}

#pragma mark - Actions

- (IBAction)refreshList
{
    self.title = @"";
    self.weather = nil;
    [self.tableView reloadData];
    [self.weatherService retrieveWeatherDataAndOnCompletion:^(WTWeatherReport *weather) {
        [NSOperation onDisplayThreadPerformBlock:^{
            self.weather = weather;
            [self.tableView reloadData];
        }];
    } onFailure:^(NSError *error) {
        [NSOperation onDisplayThreadPerformBlock:^{
            NSString* message = [NSString stringWithFormat:@"Unable to connect to weather service. (Reason: %@)",
                                 [error localizedDescription]];
            [[[UIAlertView alloc] initWithTitle:@"Connection Failure"
                                       message:message
                                      delegate:nil
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil]
             show];
        }];
    }];
}

- (IBAction)clientTapped:(id)sender
{
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.weather) {
        if (section == 0) {
            return (self.weather.currentConditions != nil) ? 1 : 0;
        } else {
            return (NSInteger)[self.weather.forecasts count];
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"WeatherCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    WTWeatherConditions *conditions = nil;
    switch (indexPath.section) {
        case 0: {
            conditions = self.weather.currentConditions;
            break;
        }
        case 1: {
            conditions = self.weather.forecasts[(NSUInteger)indexPath.row];
            break;
        }
        default:
            break;
    }
    
    cell.textLabel.text = conditions.weatherDescription;
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
}

@end
