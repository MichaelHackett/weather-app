//
//  WTWeather.m
//  Weather
//

#import "WTWeather.h"

@implementation WTWeatherConditions

@end



@implementation WTWeatherReport

- (instancetype)init
{
    self = [super init];
    if (self) {
        _forecasts = [NSMutableArray array];
    }
    return self;
}

- (WTWeatherConditions*)forecastForDayByIndex:(NSUInteger)dayIndex
{
    return _forecasts[dayIndex];
}

@end
