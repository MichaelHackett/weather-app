//
//  WTFakeWeatherService.m
//  Weather
//

#import "WTFakeWeatherService.h"

#import "WTWeather.h"
#import <Foundation/Foundation.h>


@implementation WTFakeWeatherService

- (void)retrieveWeatherDataAndOnCompletion:(void (^)(WTWeatherReport*))completionHandler
                                 onFailure:(void (^)(NSError*))failureHandler
{
    // Create dummy report to test without network.
    WTWeatherConditions* currentConditions = [[WTWeatherConditions alloc] init];
    currentConditions.weatherDescription = @"Sunny";
    currentConditions.tempC = @(24);
    
    WTWeatherConditions* forecast1 = [[WTWeatherConditions alloc] init];
    forecast1.weatherDescription = @"Cloudy";
    forecast1.tempMinC = @(-5);
    forecast1.tempMaxC = @(17);
    
    WTWeatherConditions* forecast2 = [[WTWeatherConditions alloc] init];
    forecast2.weatherDescription = @"Light rain shower";
    forecast2.tempMinC = @(7);
    forecast2.tempMaxC = @(23);
    
    WTWeatherReport* weather = [[WTWeatherReport alloc] init];
    weather.currentConditions = currentConditions;
    weather.forecasts = @[forecast1, forecast2];

    completionHandler(weather);
}

@end
