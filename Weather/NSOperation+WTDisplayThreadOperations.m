//
//  NSOperation+WTDisplayThreadOperations.m
//  Weather
//

#import "NSOperation+WTDisplayThreadOperations.h"

@implementation NSOperation (WTDisplayThreadOperations)

+ (void)onDisplayThreadPerformBlock:(void (^)(void))block
{
    [[NSOperationQueue mainQueue] addOperation:
        [NSBlockOperation blockOperationWithBlock:block]];
}

@end
