//
//  NSOperation+WTDisplayThreadOperations.h
//  Weather
//
//  Convenience method for scheduling a block to run on the display thread.
//

#import <Foundation/Foundation.h>

@interface NSOperation (WTDisplayThreadOperations)

+ (void)onDisplayThreadPerformBlock:(void (^)(void))block;

@end
