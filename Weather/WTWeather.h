//
//  WTWeather.h
//  Weather
//
//  The common model for weather data (same format for all data sources).
//

#import <Foundation/Foundation.h>


@interface WTWeatherConditions : NSObject

@property (copy, nonatomic) NSNumber* cloudCover;
@property (copy, nonatomic) NSNumber* humidity;
@property (copy, nonatomic) NSNumber* precipMM;
@property (copy, nonatomic) NSNumber* pressureKPa;
@property (copy, nonatomic) NSNumber* tempC;
@property (copy, nonatomic) NSNumber* tempF;
@property (copy, nonatomic) NSNumber* tempMinC;
@property (copy, nonatomic) NSNumber* tempMinF;
@property (copy, nonatomic) NSNumber* tempMaxC;
@property (copy, nonatomic) NSNumber* tempMaxF;
@property (copy, nonatomic) NSNumber* visibility;
@property (copy, nonatomic) NSNumber* weatherCode;
@property (copy, nonatomic) NSString* windDir16Point;
@property (copy, nonatomic) NSNumber* windDirDegree;
@property (copy, nonatomic) NSNumber* windSpeedKmph;
@property (copy, nonatomic) NSNumber* windSpeedMph;
@property (copy, nonatomic) NSString* weatherDescription;
@property (copy, nonatomic) NSString* weatherIconURL;
@property (copy, nonatomic) NSDate* date;
@property (copy, nonatomic) NSString* observationTime;

@end


@interface WTWeatherReport : NSObject

@property (strong, nonatomic) WTWeatherConditions* currentConditions;  // should be copy
@property (copy, nonatomic) NSArray* forecasts;  // array of WTWeatherConditions

- (WTWeatherConditions*)forecastForDayByIndex:(NSUInteger)dayIndex;

@end
